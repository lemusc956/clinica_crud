$(document).ready(inicio);

//FUNCION INICIO
function inicio() {
    datos();
}

//CARGANDO DATOS A TABLA ESPECIALIDADES
function datos() {
    $.ajax({
        url: "/paciente/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");

            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td>" + response[i].id + "</td>" +
                    "<td>" + response[i].nombre + "</td>" +
                    "<td>" + response[i].direccion + "</td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].id +
                    ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i> Editar</button>" +
                    "<button onclick='setIdPersona(" + response[i].id +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> Eliminar</button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function () {
            alert("Error");
        }
    });
};